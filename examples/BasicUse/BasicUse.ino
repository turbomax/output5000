// import library
#include <Output.h>

// create output-object
Output output(Serial);

void setup()
{   
    // start serial communication
    Serial.begin(115200);

    // call once in setup
    output.init();

    // f�r testing: change the interval of new data coming in (doesn't affect ping)
    output.setInterval(200);
}

void loop()
{
    // call this as often as possible (every loop)
    output.update();

    // has there been a ping event?
    if (output.pingAvailable())
    {   
        // do something
        Serial.println("ping");
    }

    // has there been a data event?
    if (output.dataAvailable())
    {   
        // do something with it
        int data = output.getData();
        Serial.println(data);
    }
}