# OUTPUT5000 Library v2

*ABK Stuttgart |�2018 / 2019*

## UPDATE

This library now offers two modes:

**online** and **offline**.

### How to use

### Step 1
Replace your current library folder in your Arduino Folder 

(usually Documents -> Arduino -> Libraries)

with the one provided [here](https://bitbucket.org/turbomax/output5000/downloads/).


### Step 2

Replace the line

````
Output output;
````
with 
````
Output output(Serial);
````

### Step 3

We need to replace the line of code in your script which reads:

```
output.init();
```

For testing purpses replace it with:
```
output.init(false);
```

For presentation mode replace it with:
```
output.init(true);
```

### Step 4
**You're done!**


----

## How to install

Download this folder, unzip it and move it into your Folder for your Arduino Libraries (usually Documents -> Arduino -> Libraries)

## How to use

Have a look at the examples-folder - there is a project, that should get you started easily. The following functions / methods are available:

**Output output;    **

Will initialize the library and create an output-object, that will take care of everything in the background =)

**output.pingAvailable();**

Will give back *true* or *false* whether there has been a ping or not

**output.dataAvailable();**

Will give back *true* or *false* whether there has been data input or not

**output.getData();**

Will give back the last data that came in

**output.update();**

Call this as often as possible, so new data and pings can be received

**output.setInterval(** *enter new interval here* **);**

With this you can change the interval of new data coming in (just for testing purposes)

