#ifndef output_h
#define output_h

// core
#include <Arduino.h>

// 3rd party libraries
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <WebSocketsClient.h>
#include <Hash.h>

class Output
{
public:
  Output(Print &print);
  void init(boolean online);
  boolean pingAvailable();
  boolean dataAvailable();
  byte getData();
  void update();
  void setInterval(unsigned int newInterval);

private:
  String serverAdress;
  void webSocketEvent(WStype_t type, uint8_t *payload, size_t length);
  boolean pingIsAvailable;
  boolean dataIsAvailable;
  unsigned long pingTarget;
  unsigned long dataTarget;
  unsigned int pingDelta;
  unsigned int dataDelta;
  byte currentData;
  
  void processData(WStype_t type, uint8_t * payload, size_t length);
  ESP8266WiFiMulti WiFiMulti;
  WebSocketsClient webSocket;
  boolean isOnline;
  Print* printer;
  boolean isValid();
};

#endif