#include <Arduino.h>

// custom library
#include <Output.h>

// 3rd party libraries
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <WebSocketsClient.h>
#include <Hash.h>

String serverAdress = "192.168.178.44";
ESP8266WiFiMulti WiFiMulti;
WebSocketsClient webSocket;

boolean isOnline;

unsigned long lastMessageTime;
unsigned int messageDelta = 100;

Output::Output(Print &print)
{
    printer = &print;
    pingIsAvailable = false;
    pingTarget = 0;
    pingDelta = 1500;

    dataIsAvailable = false;
    currentData = 0;
    dataTarget = 0;
    dataDelta = 1000;
}

void Output::webSocketEvent(WStype_t type, uint8_t *payload, size_t length)
{

    switch (type)
    {
    case WStype_DISCONNECTED:
        //USE_SERIAL.printf("[WSc] Disconnected!\n");
        break;
    case WStype_CONNECTED:
    {
        printer->printf("[WSc] Connected to url: %s\n", payload);
    }
    break;
    case WStype_TEXT:
        //printer->printf("[WSc] get text: %s\n", payload);

        if (isValid())
        {
            char text[length];

            for (int i = 0; i < length; i++)
            {
                text[i] = payload[i];
            }

            if (length == 4)
            {
                char ping[] = "ping";
                byte counter = 0;
                for (int i = 0; i < 4; i++)
                {
                    if (ping[i] == text[i])
                    {
                        counter++;
                    }
                }
                if (counter == 4)
                {
                    pingIsAvailable = true;
                }
            }
            else
            {
                byte result = atoi(text);
                currentData = result;
                dataIsAvailable = true;
            }
            lastMessageTime = millis();
        }

        break;
    case WStype_BIN:
        printer->printf("[WSc] get binary length: %u\n", length);
        hexdump(payload, length);
        break;
    }
}

void Output::init(boolean online)
{
    isOnline = online;

    if (isOnline)
    {

        for (uint8_t t = 4; t > 0; t--)
        {
            delay(1000);
        }
        WiFiMulti.addAP("ROBOTER_WERKSTATT", "50005000");
        printer->println("connecting to wifi");
        while (WiFiMulti.run() != WL_CONNECTED)
        {
            delay(100);
            printer->println("...");
        }

        webSocket.begin("192.168.178.63", 8080, "/");
        webSocket.onEvent([&](WStype_t t, uint8_t *p, size_t l) {
            webSocketEvent(t, p, l);
        });
        webSocket.setReconnectInterval(5000);
    }
}

boolean Output::pingAvailable()
{
    if (pingIsAvailable)
    {
        pingIsAvailable = false;
        return true;
    }
    else
    {
        return false;
    }
}

boolean Output::dataAvailable()
{
    if (dataIsAvailable)
    {
        dataIsAvailable = false;
        return true;
    }
    else
    {
        return false;
    }
}

byte Output::getData()
{
    return currentData;
}

void Output::update()
{
    if (isOnline)
    {
        if (WiFi.status() == WL_CONNECTED)
        {
            webSocket.loop();
        }
        else
        {
            webSocket.disconnect();
        }
    }
    else
    {
        unsigned long currentMillis = millis();

        if (currentMillis > pingTarget)
        {
            pingTarget = currentMillis + random(pingDelta);
            pingIsAvailable = true;
        }

        if (currentMillis > dataTarget)
        {
            dataTarget = currentMillis + dataDelta;
            currentData = random(256);
            dataIsAvailable = true;
        }
    }
}

void Output::setInterval(unsigned int newInterval)
{
    dataDelta = newInterval;
}

boolean Output::isValid()
{
    return millis() >= (lastMessageTime + messageDelta);
}