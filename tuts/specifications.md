# 🤖 Specifications for serial communication 🤖

For every note/sound you create should also be represented by a message you send via the USB cable to the computer.

Below you will find an example on how to do this.

## 🎓 Value mapping
The values can only range from 0-255. You are probably working with different values in your sketch, so we need to prepare the data first.

You can use the *map* function to remap your values to another range. It works like this:

**map** ( your value , min value of current range, max value of current range , min target range, max target range)

### Example
```C++
int yourValue = 354; //your values range from 0 - 1500
int valueYouSendOut = map(yourValue,0,1500,0,255); // results to 60
```

## ✈️ Sending your data
You will send the data via serial to the computer. This is pretty straightforward and you've probably used it before.

**Serial.println** (your value you want to send)

Don't forget to initialize the Serial interface in setup()! *see below*

### Example
```C++
void setup(){
    Serial.begin(115200); // this number is the speed of the communication. Don't change that!
}

void loop(){
    int yourValue = 200;
    Serial.println(yourValue);
}
```

## Complete Example

```C++
void setup(){
    Serial.begin(115200); // this number is the speed of the communication. Don't change that!
}

void loop(){
    int yourValue = random(5000); // this will just generate a random number between 0 and 5000
    int valueYouWantToSend = map(yourValue,0,5000,0,255);
    Serial.println(yourValue);
}
```